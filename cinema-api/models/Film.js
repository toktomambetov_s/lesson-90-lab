const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const FilmSchema = new Schema({
    title: {
        type: String,
        required: true,
    },
    price: {
        type: Number,
        required: true
    },
    description: String,
    image: String,
    category: {
        type: Schema.Types.ObjectId,
        ref: 'Category',
        required: true
    }
});

const Film = mongoose.model('Film', FilmSchema);

module.exports = Film;