const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const ObjectId = require('mongodb').ObjectId;

const Film = require('../models/Film');

const config = require('../config');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = db => {

    router.get('/', (req, res) => {
        Film.find().populate('category')
            .then(films => res.send(films))
            .catch(() => res.sendStatus(500));
    });

    router.post('/', [auth, permit('admin'), upload.single('image')], (req, res) => {
        const filmData = req.body;

        if (req.file) {
            filmData.image = req.file.filename;
        } else {
            filmData.image = null;
        }

        const film = new Film(filmData);

        film.save()
            .then(film => res.send(film))
            .catch(error => res.status(400).send(error));
    });

    router.get('/:id', (req, res) => {
        db.collection('films')
            .findOne({_id: new ObjectId(req.params.id)})
            .then(result => {
                if (result) res.send(result);
                else res.status(404).send({error: 'Not found'});
            })
            .catch(() => res.sendStatus(500));
    });

    return router;
};

module.exports = createRouter;