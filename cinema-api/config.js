const path = require('path');

const rootPath = __dirname;

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, 'public/uploads'),
    db: {
        url: 'mongodb://localhost:27017',
        name: 'cinema'
    },
    jwt: {
        secret: 'some kinda very secret string',
        expiresIn: 300
    }
};